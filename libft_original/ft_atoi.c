#include "libft.h"

int        ft_atoi(const char *str)
{
    int        res;
    int        neg;
	
    res = 0;
    neg = 0;
    while (*str < 33 || *str > 126)
        str++;
    if (*str == '-' || *str == '+')
    {
        if (*str == '-')
            neg = 1;
        str++;
    }
    while (*str !='\0' && *str >= '0' && *str <= '9')
    {
        if (neg)
            res = res * 10 - (*str - '0');
        else
            res = res * 10 + (*str - '0');
        str++;
    }
    return (res);
}