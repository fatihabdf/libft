/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fboudyaf <fboudyaf@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 00:22:13 by fboudyaf          #+#    #+#             */
/*   Updated: 2018/05/10 11:53:14 by fboudyaf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_bzero(void *str, size_t n)
{
	unsigned char *cast;
	size_t i;

	i = 0;
	cast = (unsigned char *)str;
	while(i < n)
	{
		cast[i] = 0; // i could also type in ((unsigned char *)str)[i] = 0 without having to create the variable cast //
		i++;
	}
}