#include <stdio.h>
#include "libft.h"

int find_digit_nb(int n);

char * ft_itoa(int n)
{
    char *str;
    int i;
    int neg;
  
    i = 0;
    neg = 0;
    if(n < 0)
        neg = 1;
    str = ft_memalloc(find_digit_nb(n));
    while(i < find_digit_nb(n))
        i++;
    str[i--] = '\0';
    while(i > neg)
    {
        if(n < 0)
            n = -n;   
        str[i--] = (n % 10) + 48;
        n = n / 10;
    }
    str[neg] = n + 48;
    if(neg == 1)
        str[--i] = '-'; 
    return(str);
}

int find_digit_nb(int n) // find nb of digit in n and allocate space 
{
    int digit_nb;

    digit_nb = 1; // there is at least 1 digit
    if(n < 0)
       digit_nb = 2; // dig_nb = space to allocate. SO far i need to allocate 1 more spce for '-'
    while(n > 9 || n < -9)
    {
        n = n / 10;
        digit_nb++;
    }  
    return(digit_nb);
}

int main()
{
    int n = -5431548;
    printf("%s\n",ft_itoa(n));
}