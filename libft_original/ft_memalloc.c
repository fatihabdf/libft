#include "libft.h"

void * ft_memalloc(size_t size)
{
    char *mem;

    mem = (char *)malloc(sizeof(*mem) * size + 1);
    if(mem == 0)
        return(NULL);
    ft_bzero(mem, size); // je peux aussi ft_memset(mem, 0, size)
    return(mem); // ici je peux retourner mem qui est un void car je l'ai caster en void
}
