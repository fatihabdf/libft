/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fboudyaf <fboudyaf@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 11:19:14 by fboudyaf          #+#    #+#             */
/*   Updated: 2018/05/11 16:14:30 by fboudyaf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "stdio.h"
#include <string.h>

void	*ft_memccpy(void *restrict dst, const void *restrict src, int c,
					size_t n)
{
	size_t			i;
	unsigned char	*src1;
	unsigned char	*dst1;

	i = 0;
	src1 = (unsigned char *)src;
	dst1 = (unsigned char *)dst;
	while (i < n)
	{
		dst1[i] = src1[i];
		if (src1[i] == (unsigned char)c)
		{
			return (dst + i + 1);
		}
		i++;
	}
	return (NULL);
}


int main()
{
	char src[100] = "fatiha";
	char dst[100];
	int c = 'w';
	char src1[100] = "fatiha";
	char dst1[100];
	int c1 = 'w';
	printf("%s\n", ft_memccpy(dst, src, c, 6));
	printf("%s\n", memccpy(dst1, src1, c1, 6));
}