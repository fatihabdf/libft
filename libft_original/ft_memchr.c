/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fboudyaf <fboudyaf@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/20 14:44:17 by fboudyaf          #+#    #+#             */
/*   Updated: 2018/05/11 16:38:44 by fboudyaf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

void *ft_memchr(const void *str, int c, size_t n)
{
	size_t i;
	unsigned char *str1;

	i = 0;
	str1 = (unsigned char*)str;
	while(str1[i])
	{
		while(i++ < n)
		{
			if (str1[i] == (unsigned char)c)
				return(str1 + 1);
		}
	}
	return(NULL);
}

int main()

	char* str = "fatihaboudyaf";
	char c = 'b';
	printf("%s\n", ft_memchr(str, c, 10));
	printf("%s\n", memchr(str, c, 10));
}
	
