/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fboudyaf <fboudyaf@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/19 01:12:01 by fboudyaf          #+#    #+#             */
/*   Updated: 2018/05/10 11:43:34 by fboudyaf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void *ft_memcpy(void *restrict dst, const void *restrict src, size_t n)
//A void pointer is a pointer that has no associated data type with it. A void pointer can hold address of any type and can be typcasted to any type.
//A const void * points to memory that should not be modified(we talk about the value of whats inside the memory). Here dst should not overlap on src means src should not change, thats why it is a const void (If dst and src overlap, behavior is undefined). 
// A void * (non-const) points to memory that could be modified (but not via the void *; you'd have to cast it first)
//we also can cast a const void. Here we need to cast the src it in order to copy it (we copy, we don't change the value of it) 
// but in that case the const is only a way to say that we should not modify the valy, the fonction asks us to copy it only so le problem ne se pose pas. 
// restrict : ici pointeur restreint sur void : L’idée est d’avoir un unique pointeur (le pointeur restreint) vers une zone mémoire qui lui est attitrée. Tant qu’il existe (tant que l’on se trouve dans la durée de vie de la variable (ici src) correspondante), aucun autre pointeur ne peut être utilisé pour accéder au même objet en mémoire… à moins d’en avoir hérité le titre. En effet, affecter un pointeur (ou le résultat d’une opération sur celui-ci) à un autre lui transfère non seulement la valeur, mais également les droits d’accès du premier. voir http://huoc.org/pointeurs-restrict.html 
{
	size_t	i;

	i = 0;
	while(i < n)
	{
		((unsigned char *)dst)[i] = ((unsigned char *)src)[i];
		i++;
	}
	return(dst);
}
	
	
