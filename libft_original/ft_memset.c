/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fboudyaf <fboudyaf@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 20:47:08 by fboudyaf          #+#    #+#             */
/*   Updated: 2018/05/10 11:48:45 by fboudyaf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void *ft_memset(void *str, int c, size_t n) 
// void without * = the function returns nothing. but void* = a pointer to a void. means it returns something : it points to a void = pointsto an adress in the memory but doesnt specify if its an int, a char, etc) //
{
	size_t			i; 		// why size_t ? //
	unsigned char	*cast; 	// unsigned char 0 -> 255 //   

	i = 0;
	cast = (unsigned char *)str; 
	// str est un void et on ne peux pas manipuler un void, je peux pas le referencer (referencing is accessing the value of the variable that the pointer points to a th &). donc ci dessus je cast str = je le transforme en unsigned char. J'aurais pu faire just cast = str mais pas bon car ici ptr est un char et str est un void : -> erreur //  
	while(i < n) 
// n est le nombre de caractere que je veux remplacer. Quand n = 0, je suis situe au premier char que je veux remplacer  //
	{
		cast[i] = c; // cast[i] ~= str[i] mais c la version castee cad la version que je peux manipuler //
		i++; 
	}
	return(str); // pourquoi pas return(cast) ?? car la fonction memset retourn son premier argument, ici str + la fonction doit retourner un void//
}

