#include "libft.h"
#include <stdio.h>

int ft_pow(int x, int n)
{
    if(n == 0)
        return 1;
    else
        return(x * ft_pow(x, n-1));
}

int main()
{
    int n = 4;
    int x = 8;
    printf("%d\n",ft_pow(x, n));
}