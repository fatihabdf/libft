#include "libft.h"
#include <stdio.h>

int ft_pow2(int x, int n)
{
    int y; 

    if(n == 0)
        return 1;
    else if(n % 2 == 0)
    {
        y = ft_pow2(x, n/2);
        return(y * y);
    }
    else
        return(x * ft_pow2(x, n-1));
}

int main()
{
    int n = 4;
    int x = 8;
    printf("%d\n",ft_pow2(x, n));
}