#include "libft.h"
#include <stdio.h>

void ft_putendl(char const *s)
{
    printf("%s", s);
    while(*s)
    {
        write(1, s, 1);
        s++;
    }
    write(1, "\n", 1);
}

int main ()
{
    char *str = "fatiha";
    ft_putendl(str);
}