#include "libft.h"

void ft_putendl_fd(char const *s, int fd)
{
    while(*s)
    {
        write(fd, s, 1);
        s++;
    }
    write(fd, "\n", 1);
}

int main()
{
    int fd = 1;
    const char *s = "fatiha";
    ft_putendl_fd(s, fd);
}