#include "libft.h"
#include <stdio.h>

void ft_putstr(char const *s)
{
    while(*s)
    {
        write(1, s, 1);
        s++;
    }
}

int main()
{
    char *str = "fatiha";
    ft_putstr(str);
}

