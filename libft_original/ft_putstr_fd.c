#include "libft.h"

void ft_putstr_fd(char const *s, int fd)
{
    while(*s)
    {
        write(fd, s, 1);
        s++;
    }
}
int main()
{
    int fd = 1;
    const char *s = "fatiha";
    ft_putstr_fd(s, fd);
}