#include "libft.h"

char *ft_strchr(const char *s, int c)
{
    char *ss;

    ss = (char *)s;
    while(*ss != c)
    {
        if(*ss == '\0')
            return(NULL);
        ss++;
    }
    return(ss);
}

