#include "libft.h"

void    ft_strclr(char *s)
{
     int i;

     i = 0;
     while(s[i] != '\0' && s != NULL) // s != NULL check si le string exist. Car si le string s == NULL donc s n'existe pas alors le pointeur * pointe sur rien
    {
        s[i] = '\0';  // ou alors *s++ = '\0';}
        i++;
    }
}