/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fboudyaf <fboudyaf@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/20 19:31:06 by fboudyaf          #+#    #+#             */
/*   Updated: 2018/05/10 11:44:15 by fboudyaf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char *ft_strdup(char *str)
{
	char *dup;
	int i;
	int len;

	i = 0;
	len = 0;
	while(str[len])
		len++;
	dup = (char *)malloc(sizeof(*str) * len); // si je mets ici len + 1 j'ai pas besoin de la ligne 31 car je copie deja le '\0' du str en plus
	while(i < len) // ou while(str[i] != '\0')
	{
		dup[i] = str[i];
	   	i++;
	}
	dup[i] = '\0'; // ou dup[len] = '\0'
	return(dup);
}	
