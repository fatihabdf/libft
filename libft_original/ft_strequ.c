#include "libft.h"

int     ft_strequ(char const *s1, char const *s2)
{
    while(*s1 && *s2)
    {
        if(*s1 != *s2)
            return 0;
        s1++;
        s2++;
    }
    if(*s1 == *s2)
    {
        return 1;
    }
    return 0;            
}

int main()
{
    char const str1[20] = "fatiha";
    char const str2[20] = "fati";
    int ret = ft_strequ(str1, str2);
    if(ret == 0)
        write(1, "0", 1);
    else
        write(1, "1", 1);
    return(0);
}