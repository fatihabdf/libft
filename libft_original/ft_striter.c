#include "libft.h"

void ft_striter(char *s, void (*f)(char *)) //(*f)(char*) = pointeur sur la fonction f 
// qui prends en parametre un char * (ici char *s)  
{
    if(s != NULL && f != NULL)
    {
        while(*s)
        {
            f(s);
            s++;
        }  
    }
}

void    ft_pputchar(char *c)
{
    write(1, c, 1); // ici c c'est l'adresse de str du main, donc pas besoin d'ajouter &. 
                    // alors que *c c'est la valeur du str '
    write(1, " ", 1);
    write(1, "...", 3);
}

int main ()
{
    char *str = "fatiha";
    ft_striter(str, ft_pputchar); // avec ft_striter, j'applique la fonction du 
    //second param sur chaque charactere (1er param)..
}
