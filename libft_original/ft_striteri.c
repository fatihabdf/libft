#include "libft.h"

/*void ft_striteri(char *s, void (*f)(unsigned int, char *))
{
    unsigned int i;

    i = 0;
    if(s != NULL && f != NULL)
    {
        while(*s)
        {
            f(i, s); 
            s++; --> ici j'incerement 1premiere fois'
            i++; --> ici j'incremente une deuxieme fois donc FAUX'
        }
    }
}
*/
void ft_striteri(char *s, void (*f)(unsigned int, char *))  //la seule dif ici avec striter c'est qu'on utolise un index. 
                                                            //mais c'est la meme fonction avec le meme objectif'
{
    unsigned int i;
    i = 0;
    if(s != NULL && f != NULL)
    {
        while(s[i]) // ici au lieu de *s on met s[i]
        {
            f(i, s);
            i++;
        }
    }
}

void ft_jsp(unsigned int i, char *c)
{
    write(1, &(c[i]), 1); // ici on est oblige utiliser c[i] car on doit utiliser un index. Mais c[i] ne peux pas etre ladress 
                          // mais la valeur du char. je suis donc oblige d'ajouter le &.
    write(1, "...", 3);
}

int main()
{
    char str[7] = "fatiha";
    ft_striteri(str, ft_jsp);
    return(0);
}
















