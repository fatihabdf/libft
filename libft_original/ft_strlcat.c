#include <stdio.h>
#include "libft.h"
size_t  ft_strlcat(char * restrict dst, const char * restrict src, size_t dstsize)

{
    size_t len;
    size_t i;

    len = 0;
    i = 0;
    while(dst[len])
        len++;
    while(src[i] &&  i < dstsize)
    {
        dst[len] = src[i];
        i++;
        len++;
    }
    dst[len] = '\0';
    return(len);

	int main()
    {
    char str9[100] = "fatiha";
	char strr9[100] = "boudyaf";
	char str10[100] = "fatiha";
	char strr10[100] = "boudyaf";
	printf("ft_strlcat : %zu\n", ft_strlcat(str9, strr9, 5));
	printf("strlcat : %zu\n", strlcat(str10, strr10, 5));
    }
