#include "libft.h"

char*   ft_strmap(char const *s, char (*f)(char))
{
    char* dup;
    dup = (char*)malloc(sizeof(*s) * strlen(s) + 1);

    if(s != NULL && f != NULL)
    {
        while(*s)
        {
            *dup = f(*s);
            s++;
            dup++;
        }

    }
    return(dup);
}

char ft_pputchar(char c)
{
    write(1, &c, 1);
    return(c);
}

int main()
{
    const char* str = "fatiha";
    ft_strmap(str, ft_pputchar);
}
