#include "libft.h"

char * ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char* dup;
	unsigned int i;

	dup = (char *)malloc(sizeof(*s) * strlen(s) + 1);
	i = 0;
	if(s != NULL && f != NULL)
	{
		while(s[i])
		{
			*dup = f(i , s[i]);
			i++;
			dup++;
		}
	}
	return(dup);
}

char ft_jsp(unsigned int i, char c)
{
	write(1, &c, 1);
	return(c);
}

int main()
{
	char *str = "fatiha";
	ft_strmapi(str, ft_jsp);
}