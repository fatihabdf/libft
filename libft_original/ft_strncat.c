#include <stdio.h>
#include "libft.h"

char    *ft_strncat(char *restrict s1, const char *restrict s2, size_t n)
{
    int len1;
    size_t i;

    len1 = 0;
    i = 0;
    while(s1[len1])
    {
        len1++;
    }
    while(s2[i] && i < n )
    {
        s1[len1] = s2[i];
        len1++;
        i++;
    }
    s1[len1] = '\0';
    return(s1);
}

int main()
{
    char s1[20] =  "fatiha";
    char s2[20] = "boudyaf";
    char s3[20] =  "fatiha";
    char s4[20] = "boudyaf";
    printf("%s\n", strncat(s1, s2, 3));
    printf("%s\n", ft_strncat(s3, s4, 3));
}