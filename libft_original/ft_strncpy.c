/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fboudyaf <fboudyaf@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/20 20:27:24 by fboudyaf          #+#    #+#             */
/*   Updated: 2018/05/12 14:47:34 by fboudyaf         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft.h"

char *ft_strncpy(char *dst, const char *src, size_t len)
{
    size_t i;

    i = 0;
    while(src[i] != '\0' && (i < len)) // i < len pour le cas ou si len = 0 donc i = len et dans c cas il n'entre pas dans la loop du tout et il return direct dst qui est null. Si je le met pas, alors il rentre dans la loop et va aller direct dans dst[i] = '\0' et donc vas retourner dst donc '\0'.
    {
        dst[i] = src[i];
        i++;
    }
    while(i < len)
        dst[i++] = '\0';
    dst[i] = '\0';
    return(dst);
}

int main()
{
    char *str = "fatihaboudyaf";
    char dst[25];
    printf("%s\n", ft_strncpy(dst, str, 8));
}
