#include "libft.h"

int ft_strnequ(char const *s1, char const *s2, size_t n)
{
    size_t i;

    i = 0;
    while(s1[i] && i < (n - 1))
    {
        if(s1[i] != s2[i])
            return(0);
        else
            i++;
    }
    if(s1[i] == s2[i])
    {
        return(1);
    }
    return(0);
}

int main()
{
    char const str[20] = "fatiha";
    char const str1[20] = "fatpha";
    int ret = ft_strnequ(str, str1, 3);
    if(ret == 0)
        write(1, "0", 1);
    else
        write(1, "1", 1);
}