#include "libft.h"
#include <stdio.h>

char * ft_strnew(size_t size)
{
    char *mem;
    size_t i;

    i = 0;
    mem = (char *)malloc(sizeof(*mem) * (size + 1));
    if(mem == 0)
        return(NULL);
    while(i < size)  // ou alors ft_memset(mem, '\0', size + 1)
    {
        mem[i] = '\0';
        i++;
    }
    mem[i] = '\0';
    return(mem);
}

int main()
{
    size_t n = 8;
    printf("%s\n",ft_strnew(n));
}

