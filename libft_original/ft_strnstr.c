#include "libft.h"
#include <stdio.h>

char	*ft_strnstr(const char *s1, const char *find, size_t n)
{
	size_t	i;

	if (*find == '\0')
		return ((char*)s1);
	i = 0;
	while (*s1 && n)
	{
		if (*s1 == find[i])
			i++;
		else
			i = 0;
		if (find[i] == '\0')
			return ((char*)(s1 - i + 1));
		s1++;
		n--;
	}
	return (NULL);
}

 int main()
 {
	 char *s1 = "fatihaboudyaf";
	 char *s2 = "boudyaf";
	 printf("%s\n", ft_strnstr(s1, s2, 13));
	 printf("%s\n", strnstr(s1, s2, 13));
 }