#include "libft.h"
#include <stdio.h>

char *ft_strrchr(const char *s, int c)
{
    char    *ss;
    int     i;

    ss = (char *)s;
    i = 0;  
    if(*ss == '\0')
        return(NULL);
    while(ss[i] != '\0')
        i++;
    while(i > 0)
    {
      while(ss[i] != c && i > 0)
      {
        i--;
      }
      if(ss[i] == c)
        return(ss + i);
    }
    return(NULL);
}

int main()
{
    char *str = "helloWorld";
    char c = '\0';
    printf("%s\n", ft_strrchr(str, c));
    printf("%s\n", strrchr(str, c));
}