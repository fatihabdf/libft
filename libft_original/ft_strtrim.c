#include "libft.h"
 #include <stdio.h>


 #define INIT int i; int j; int start; int end; int tot_blank

 static int        count_beg_space(const char *str)
 {
     int count;

     count = 0;
     while (*str)
     {
         while (*str == ' ' || *str == '\n' || *str == '\t')
         {
             count++;
             str++;
         }
         return (count);
     }
     return (0);
 }

 static int        count_end_space(const char *str)
 {
     int count;
     int i;

     i = 0;
     count = 0;
     while (str[i])
         i++;
     while (i > 0 && (str[i - 1] == ' ' || str[i - 1] == '\n' || str[i - 1] == '\t'))
     {
         count++;
         i--;
     }
     return (count);
 }

 char            *ft_strtrim(char const *s)
 {
     char    *dup;

     INIT;
     i = 0;
     j = 0;
     if (!s)
         return (NULL);
     start = count_beg_space(s);
     if (start == (int)ft_strlen(s))
         return ("\0");
     tot_blank = start + count_end_space(s);
     end = ft_strlen(s) - tot_blank + start;
     if (!(dup = (char *)malloc(sizeof(char) * (ft_strlen(s) - tot_blank + 1))))
         return (NULL);
     while (s[i])
     {
         if (i >= start && i < end)
             dup[j++] = s[i++];
         else
             i++;
     }
     dup[j] = '\0';
     return (dup);
 }




 #define INIT  int i; int len; int start; char *dup

 char    *ft_strtrim(char const *s)
 {
     INIT;
     i = 0;
     if (!s)
         return (NULL);
     len = ft_strlen(s);
     while (s[len - 1] == ' ' || s[len - 1] == '\n' || s[len - 1] == '\t')
         len--;
     while (s[i] == ' ' || s[i] == '\n' || s[i] == '\t')
     {
         len--;
         i++;
     }
     start = i;
     if (start == (int)ft_strlen(s))
         return ("\0");
     if (!(dup = (char *)malloc(sizeof(char) * (len + 1))))
         return (NULL);
     i = 0;
     while (i < len)
         dup[i++] = s[start++];
     dup[i] = '\0';
     return (dup);
 }

